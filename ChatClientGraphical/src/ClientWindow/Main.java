package ClientWindow;

import Client.ChatClient;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.List;

public class Main extends Application
{

    Stage primaryStage;
    Parent root;
    ChatClient chatClient = new ChatClient();

    TextField textField;
    Button send;
    VBox vb;
    ScrollPane scroll;

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        this.primaryStage = primaryStage;
        List<String> args = getParameters().getRaw();
        if (args.size() != 1) {
            System.out.println("Syntax - " + ChatClient.class.getName() + " host:port");
            System.exit(1);
        }

        root = FXMLLoader.load(getClass().getResource("Login.fxml"));
        primaryStage.setTitle("RMIChat");
        primaryStage.setScene(new Scene(root, 1280, 720));
        primaryStage.show();

        textField = (TextField) root.lookup("#Nick");
        send = (Button) root.lookup("#Button");

        send.setOnAction(event -> {
            if (!textField.getText().equals("")) {
                try {
                    chatClient.setNickname(textField.getText());
                    init(args.get(0));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        root.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case ENTER:
                    if (!textField.getText().equals("")) {
                        chatClient.setNickname(textField.getText());
                        try {
                            init(args.get(0));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
            }
        });
    }

    private void init(String nickname) throws Exception
    {
        chatClient.init(nickname);
        chatClient.setMain(this);

        root = FXMLLoader.load(getClass().getResource("RMIChat.fxml"));
        primaryStage.setTitle("RMIChat");
        primaryStage.setScene(new Scene(root, 1280, 720));
        primaryStage.show();

        textField = (TextField) root.lookup("#Input");
        send = (Button) root.lookup("#Send");
        vb = (VBox) root.lookup("#List");
        scroll = (ScrollPane) root.lookup("#Scroll");
        scroll.setVvalue(1);

        send.setOnAction(event -> {
            send();
        });

        root.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case ENTER:
                    send();
                    break;
            }
        });

        primaryStage.setOnCloseRequest(event -> {
            chatClient.exit();
        });
    }

    private void send()
    {
        chatClient.input(textField.getText());
        textField.setText("");
    }

    public void addEntry(String src, String msg)
    {
        HBox hb = new HBox();
        Label user = new Label(src + " : ");
        Label message = new Label(msg);

        hb.getChildren().add(user);
        hb.getChildren().add(message);

        vb.getChildren().add(hb);
        Platform.runLater(()-> {
            scroll.setVvalue(1);
        });
    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
