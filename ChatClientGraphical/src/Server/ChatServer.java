package Server;

import Client.ChatBack;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 * Représente un serveur de Server.Chat texte.
 *
 * @author gbasile001
 * @author ldumartin
 */
public class ChatServer extends UnicastRemoteObject implements Chat
{
    private Map<String, ChatBack> users;

    private ChatServer() throws RemoteException
    {
        super();
        users = new TreeMap<>();
    }

    public static void main(String args[])
    {
        if (args.length != 1) {
            System.out.println("Syntax - " + ChatServer.class.getName() + " localhost:port");
            System.exit(1);
        }

        try {
            ChatServer obj = new ChatServer(); // activation

            String host = args[0];
            String url = "rmi://" + host + "/" + ChatServer.class.getName();

            // Bind this object instance to the name "ChatServer"
            System.out.println("Bind the Remote Object to the RMI registry");
            Naming.rebind(url, obj);
            System.out.println(ChatServer.class.getName() + " bound in registry");

        } catch (Exception e) {
            System.out.println(ChatServer.class.getName() + " err: " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void connect(String nick, ChatBack ref) throws RemoteException
    {
        users.put(nick, ref);
        ref.send("Serveur", "Vous êtes connecté.");
    }

    @Override
    public void disconnect(String nick) throws RemoteException
    {
        users.remove(nick);
        users.entrySet().forEach(e -> {
            try {
                e.getValue().send("Serveur", nick + " a quitte le salon.");
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
        });
    }

    @Override
    public ArrayList<String> list() throws RemoteException
    {
        return new ArrayList<>(this.users.keySet());
    }

    @Override
    public void send(String src, String msg) throws RemoteException
    {
        users.entrySet().forEach(e -> {
            try {
                e.getValue().send(src, msg);
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
        });
    }

    @Override
    public void send(String src, String dst, String msg) throws RemoteException
    {
        ChatBack dest = users.get(dst);
        ChatBack sorc = users.get(src);
        if (dest != null) {
            dest.send(src, msg);
            sorc.send(src, msg);
        } else sorc.send(src, dst + " n'est pas connecté");
    }
}
