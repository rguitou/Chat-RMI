package Client;

import ClientWindow.Main;
import javafx.application.Platform;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Représente un callback
 */
class ChatCallBackServer extends UnicastRemoteObject implements ChatBack
{
    private Main main;

    public ChatCallBackServer() throws RemoteException
    {
        super();
    }

    public void setMain(Main main)
    {
        this.main = main;
    }

    @Override
    public void send(String src, String msg) throws RemoteException
    {
        Platform.runLater(() -> main.addEntry(src, msg));
    }
}
