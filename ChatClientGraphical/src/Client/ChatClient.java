package Client;

import ClientWindow.Main;
import Server.Chat;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Scanner;

/**
 * Représente un client du serveur de Server.Chat texte.
 */
public class ChatClient
{
    /**
     * String calling a command
     */
    private static final String commandCallString = "/";
    /**
     * String used to disconnect
     */
    private static final String disconnectString = commandCallString + "exit";
    /**
     * The remote object
     */
    private Chat server = null;
    /**
     * Callback object
     */
    private ChatCallBackServer callback;
    /**
     * User's nickname
     */
    private String nickname;
    /**
     * Main
     */
    private Main main;

    public String getNickname() {

        return nickname;
    }

    public void setNickname(String nickname)
    {
        this.nickname = nickname;
    }

    public void init(String host) {
        try {
            Scanner in = new Scanner(System.in);
            String url = "rmi://" + host + "/" + "Server.ChatServer";
            System.out.println("Look up for the Remote Object from RMI registry " + url);
            server = (Chat) Naming.lookup(url);

            System.out.println("Try to export Callback object...");
            callback = new ChatCallBackServer();
            server.connect(nickname, callback);
        } catch (Exception e) {
            System.out.println(ChatClient.class.getName() + " exception: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void input(String str) {
        String[] mots = str.split(" ");
        try {
            switch (mots[0]) {
                case commandCallString + "list":
                    Collection<String> c = server.list();
                    main.addEntry("Systeme", "Liste des utilisateurs connectés :");
                    for (String s : c) {
                        main.addEntry("Systeme", s);
                    }
                    break;
                case commandCallString + "w":
                    String message = "";
                    if (mots.length > 2) {
                        for (int i = 2; i < mots.length; i++) {
                            message += mots[i] + " ";
                        }
                        server.send(nickname, mots[1], message);
                    } else main.addEntry("Systeme", "utilisation : /w nom message");
                    break;
                case disconnectString:
                    exit();
                    break;
                default:
                    if (str.startsWith(commandCallString))
                        main.addEntry("Systeme", "Cette commande n'existe pas.");
                    else server.send(nickname, str);
                    break;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void setMain(Main main) {
        this.callback.setMain(main);
        this.main = main;
    }

    public void exit() {
        try {
            server.disconnect(nickname);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
}