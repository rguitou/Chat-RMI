// ChatServer.java

import ChatApp.*;
import org.omg.CosNaming.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

class ChatImpl extends ChatPOA {
  private ORB orb;

  Map<ChatCallback, String> users = new HashMap();


  public void setORB(ORB orb_val) {
    orb = orb_val; 
  }
    
  // implement chat() method with callback
  public String chat (String name, String message) {
    Set<Map.Entry<ChatCallback, String>> setHm = users.entrySet();
    Iterator<Map.Entry<ChatCallback, String>> it = setHm.iterator();
    while(it.hasNext()) {
      Map.Entry<ChatCallback, String> e = it.next();
      e.getKey().callback(name+" >>", message);
    }
    return name + " >> " + message;
  }
  // implement connect() method
  public void connect(ChatCallback objRef ,String name){
    users.put(objRef, name);
  }

  // implement list() method
  public void list(ChatCallback objRef) {
    objRef.callback("", "Les utilisateurs suivants sont connectés : ");
    Set<Map.Entry<ChatCallback, String>> setHm = users.entrySet();
    Iterator<Map.Entry<ChatCallback, String>> it = setHm.iterator();
    while(it.hasNext()) {
      Map.Entry<ChatCallback, String> e = it.next();
      objRef.callback(e.getValue(),"");
    }
  }

    public void disconnect(ChatCallback objRef ,String name) {

      Set<Map.Entry<ChatCallback, String>> setHm = users.entrySet();
      Iterator<Map.Entry<ChatCallback, String>> it = setHm.iterator();
      while(it.hasNext()) {
        Map.Entry<ChatCallback, String> e = it.next();
        e.getKey().callback(name,"s'est déconnecté");
      }
      users.remove(objRef);
    }

    public void whisper(ChatCallback objRef, String name, String dest, String message) {
      boolean found = false;

      Set<Map.Entry<ChatCallback, String>> setHm = users.entrySet();
      Iterator<Map.Entry<ChatCallback, String>> it = setHm.iterator();

      while(it.hasNext()) {
        Map.Entry<ChatCallback, String> e = it.next();
        if(e.getValue().equals(dest)) {
          e.getKey().callback(name, "vous murmure : "+message);
          objRef.callback(name, "murmure à "+dest+" : "+message);
          found = true;
        }
      }

      if(!found)
        objRef.callback("",dest+" n'est pas connecté");

  }

  // implement shutdown() method
  public void shutdown() {
    orb.shutdown(false);
  }
}

public class ChatServer {

  public static void main(String args[]) {
    try{
      // create and initialize the ORB
      ORB orb = ORB.init(args, null);

      // get reference to rootpoa & activate the POAManager
      POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
      rootpoa.the_POAManager().activate();

      // create servant and register it with the ORB
      ChatImpl chatImpl = new ChatImpl();
      chatImpl.setORB(orb); 

      // get object reference from the servant
      org.omg.CORBA.Object ref = rootpoa.servant_to_reference(chatImpl);
      Chat href = ChatHelper.narrow(ref);
	  
      // get the root naming context
      // NameService invokes the name service
      org.omg.CORBA.Object objRef =
          orb.resolve_initial_references("NameService");
      // Use NamingContextExt which is part of the Interoperable
      // Naming Service (INS) specification.
      NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

      // bind the Object Reference in Naming
      String name = "hello";
      NameComponent path[] = ncRef.to_name( name );
      ncRef.rebind(path, href);

      System.out.println("ChatServer ready and waiting ...");

      // wait for invocations from clients
      orb.run();
    } 
	
      catch (Exception e) {
        System.err.println("ERROR: " + e);
        e.printStackTrace(System.out);
      }
	  
      System.out.println("ChatServer Exiting ...");
	
  }
}
