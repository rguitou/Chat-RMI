// ChatClient.java
import ChatApp.*;
import java.util.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;
import java.util.Scanner;

// servant class
class ChatCallbackImpl extends ChatCallbackPOA 
{
    // implement callback() method
    public void callback(String name, String message) {

    	System.out.println(name+" "+message);
    }
}

// thread class
class ThreadRun extends Thread
{
    private ORB orb;
    
    public ThreadRun(ORB orb) {
	this.orb = orb;
    }

    public void run() {
	try{
	    orb.run();
	} catch (Exception e) {
	    System.out.println("ERROR : " + e) ;
	    e.printStackTrace(System.out);
	    System.exit(1);
	}	
    }

    public void shutdown() {
	orb.shutdown(false);
    }
}

// main class
public class ChatClient {
    
    public static void main(String args[]) {
	try{
	    // create and initialize the ORB
	    ORB orb = ORB.init(args, null);
	    
	    // get the root naming context
	    org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
	    // Use NamingContextExt instead of NamingContext. This is 
	    // part of the Interoperable naming Service.  
	    NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
 

		// get reference to rootpoa & activate the POAManager
	    POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
	    rootpoa.the_POAManager().activate();

	    // create servant and register it with the ORB
	    ChatCallbackImpl chatCallbackImpl = new ChatCallbackImpl();

	    // get object reference from the servant
	    org.omg.CORBA.Object ref = rootpoa.servant_to_reference(chatCallbackImpl);
	    ChatCallback hcbref = ChatCallbackHelper.narrow(ref);

		// resolve the Object Reference in Naming
		Scanner in = new Scanner(System.in);
		System.out.println("Veuillez choisir un pseudo:");
		String name = in.nextLine();
		System.out.println("Vous pouvez chatter ! :");
		Chat chatImpl = ChatHelper.narrow(ncRef.resolve_str("hello"));
		chatImpl.connect(hcbref, name);

	    // start the server 
	    ThreadRun thread = new ThreadRun(orb);
	    thread.start();
		String str = "";



		while (!str.equals("/disconnect")) {
			str = in.nextLine();
			String[] mots = str.split(" ");
			switch (mots[0]) {
				case "/list":
					chatImpl.list(hcbref);
					break;
				case "/w":
					String message = "";
					for (int i = 2; i < mots.length; i++) {
						message += mots[i] + " ";
					}
					chatImpl.whisper(hcbref, name, mots[1], message);
					break;
				case "/disconnect": 
					break;
				default:
					chatImpl.chat(name, str);
					break;
			}
		}


	    // shutdown
	    chatImpl.disconnect(hcbref, name);
	    thread.shutdown();
	    
	} catch (Exception e) {
	    System.out.println("ERROR : " + e) ;
	    e.printStackTrace(System.out);
	}
    }
    
}
